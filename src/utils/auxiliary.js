import { makeReadable } from "./stringTransformations";

const removeKey = (object, key) => {
  Object.keys(object).forEach(item => {
    if (item === key)
      delete object[key]
  });
  return object;
};

const createTask = name => ({
  assignedTo: "",
  name,
  label: makeReadable(name)
});

const createInitialTaskList = array =>
  array.map(item => createTask(item));

const createTeamMember = name => ({
  assignments: [],
  name,
  label: makeReadable(name)
});

// a little function to help us with reordering the result
const reorder = (list, startIndex, endIndex) => {
  const result = [...list];
  const [removed] = result.splice(startIndex, 1);
  result.splice(endIndex, 0, removed);

  return result;
};

/**
 * Moves an item from one list to another list.
 */
const move = (source, destination, droppableSource, droppableDestination) => {
  const sourceClone = [...source];
  const destClone = [...destination];
  const [removed] = sourceClone.splice(droppableSource.index, 1);

  destClone.splice(droppableDestination.index, 0, removed);

  const result = {};
  result[droppableSource.droppableId] = sourceClone;
  result[droppableDestination.droppableId] = destClone;

  return result;
};

const createDragEndHandler = (getStagesList, updateState) => result => {
  const { source, destination } = result;

  if (!destination) {
    return;
  }

  const condition = source.droppableId === destination.droppableId;

  if (condition) {
    const { droppableId, index } = source;
    const items = reorder(
      getStagesList(droppableId),
      index,
      destination.index
    );

    updateState({ [droppableId]: items });
  } else {
    const result = move(
      getStagesList(source.droppableId),
      getStagesList(destination.droppableId),
      source,
      destination
    );

    updateState({ ...result });
  }
};

const grid = 8;

const getItemStyle = (isDragging, draggableStyle) => ({
  // some basic styles to make the items look a bit nicer
  userSelect: 'none',
  padding: grid * 2,
  margin: `0 0 ${grid}px 0`,

  // change background colour if dragging
  background: isDragging ? 'lightgreen' : 'grey',

  // styles we need to apply on draggables
  ...draggableStyle
});

const getListStyle = isDraggingOver => ({
  background: isDraggingOver ? 'lightblue' : 'lightgrey',
  padding: grid,
  minWidth: 250
});

export { createTask, createDragEndHandler, createInitialTaskList, createTeamMember, getItemStyle, getListStyle, removeKey }
