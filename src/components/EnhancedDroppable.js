import React from 'react';
import { Draggable, Droppable } from "react-beautiful-dnd";
import { getItemStyle, getListStyle } from "../utils/auxiliary";
import { makeReadable } from "../utils/stringTransformations";

const EnhancedDroppable = ({ addTask, droppableId, items, removeStage, removeTask, renameStage }) => (
  <Droppable droppableId={droppableId}>
    {(provided, snapshot) => (
      <div
        ref={provided.innerRef}
        style={getListStyle(snapshot.isDraggingOver)}>
        <div>{makeReadable(droppableId)}</div>
        <button onClick={renameStage}>Rename Stage</button>
        <button onClick={removeStage}>Remove Stage</button>
        <button onClick={addTask}>Add Task</button>
        {items.map((item, index) => (
          <Draggable
            draggableId={item.name}
            index={index}
            key={item.name}
          >
            {(provided, snapshot) => (
              <div
                ref={provided.innerRef}
                {...provided.draggableProps}
                {...provided.dragHandleProps}
                style={getItemStyle(
                  snapshot.isDragging,
                  provided.draggableProps.style
                )}>
                <div>{`Implement ${item.label} feature`}</div>
                <button onClick={removeTask(item.name)}>Remove Task</button>
                <div>
                  {`Assigned to: ${item.assignedTo}`}
                </div>
              </div>
            )}
          </Draggable>
        ))}
        {provided.placeholder}
      </div>
    )}
  </Droppable>
);

export default EnhancedDroppable
