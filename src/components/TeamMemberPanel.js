import React from 'react';
import './TeamMemberPanel.css';

const TeamMemberPanel = ({ addTeamMember, teamMembers, removeTeamMember }) => (
  <div className="team-member-panel-container">
    <div>Dream Team</div>
    <button onClick={addTeamMember} className="team-member-panel-button-add">Add Team Member</button>
    <ul className="team-member-panel-list">
      {teamMembers.map((member, index) => (
        <ol key={index} className="team-member-panel-item">
          <div>{member.label}</div>
          <div>
            {`Tasks assigned: ${member.assignments}`}
          </div>
          <button>Add Assignment</button>
          <button onClick={removeTeamMember(index)}>Remove Team Member</button>
        </ol>
      ))}
    </ul>
  </div>
);

export default TeamMemberPanel
