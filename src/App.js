import React, {Component} from 'react';
import './App.css';
import { DragDropContext } from "react-beautiful-dnd";
import {
  createDragEndHandler, createInitialTaskList, createTask, createTeamMember, removeKey,
} from "./utils/auxiliary";
import TeamMemberPanel from "./components/TeamMemberPanel";
import EnhancedDroppable from "./components/EnhancedDroppable";

const tasks = [
  'add_stage', 'remove_stage', 'rename_stage', 'add_task', 'remove_task', 'move_task'
];

export default class App extends Component {
  state = {
    teamMembers: [],
    stages: {
      to_do: createInitialTaskList(tasks),
      in_progress: [],
      done: []
    }
  };

  getStagesList = id => this.state.stages[id];

  updateState = state => object =>
    this.setState(prevState => ({
      ...prevState,
      [state]: {
        ...prevState[state],
        ...object
      }
    }));

  handleDragEnd = createDragEndHandler(this.getStagesList, this.updateState("stages"));

  addTeamMember = () => {
    /* eslint-disable no-restricted-globals*/
    const name = prompt("Enter new team member name in underscore case", "john_doe");

    this.setState({
      teamMembers: [
        ...this.state.teamMembers,
        createTeamMember(name)
      ]
    });
  };

  removeTeamMember = index => () => {
    /* eslint-disable no-restricted-globals*/
    const confirmation = confirm("Are you sure?");

    if (confirmation)
      this.setState(prevState => ({
        teamMembers: prevState.teamMembers.filter((item, id) =>
          id !== index
        )
      }));
  };

  addStage = () => {
    /* eslint-disable no-restricted-globals*/
    const stage = prompt("Enter stage name in underscore case", "new_stage");
    this.setState(prevState => ({
      stages: {
        ...prevState.stages,
        [stage]: []
      }
    }));
  };

  removeStage = stage => () => {
    /* eslint-disable no-restricted-globals*/
    const confirmation = confirm("Are you sure?");
    if (confirmation)
      this.setState(prevState => ({
        ...prevState,
        stages: removeKey(prevState.stages, stage)
      }));
  };

  renameStage = stage => () => {
    /* eslint-disable no-restricted-globals*/
    const newName = prompt("Enter new stage name in underscore case", "new_name");

    this.setState(prevState => ({
      ...prevState,
      stages: {
        ...prevState.stages,
        [newName]: prevState.stages[stage]
      }
    }), this.removeStage(stage));
  };

  addTask = key => () => {
    /* eslint-disable no-restricted-globals*/
    const name = prompt("Enter new task name in underscore case", "new_task");
    this.setState(prevState => ({
      stages: {
        ...prevState.stages,
        [key]: [
          ...prevState.stages[key],
          createTask(name)
        ]
      }
    }));
  };

  removeTask = stage => name => () => {
    /* eslint-disable no-restricted-globals*/
    const confirmation = confirm("Are you sure?");
    if (confirmation)
      this.setState(prevState => ({
        stages: {
          ...prevState.stages,
          [stage]: prevState.stages[stage].filter(item =>
            item.name !== name
          )
        }
      }));
  };

  render() {
    const { stages, teamMembers } = this.state;

    const stagesKeys = Object.keys(stages);

    return (
      <div className="App">
        <TeamMemberPanel
          addTeamMember={this.addTeamMember}
          removeTeamMember={this.removeTeamMember}
          teamMembers={teamMembers}
        />
        <DragDropContext onDragEnd={this.handleDragEnd}>
          <button onClick={this.addStage} style={{ minWidth: '100px' }}>Add Stage</button>
          {stagesKeys.map(key => (
            <EnhancedDroppable
              addTask={this.addTask(key)}
              droppableId={key}
              items={this.getStagesList(key)}
              key={key}
              removeStage={this.removeStage(key)}
              removeTask={this.removeTask(key)}
              renameStage={this.renameStage(key)}
            />
          ))}
        </DragDropContext>
      </div>
    );
  }
}
